import Home from '@/module/home/page/home.vue';
import page_list from '@/module/cms/page/page_list.vue';
import page_edit from '@/module/cms/page/page_edit.vue';
import page_add from '@/module/cms/page/page_add.vue'
export default [{
    path: '/',
    component: Home,
    name: 'CMS',//菜单名称
    hidden: false,
    children:[
      {path:'/cms/page/list',name:'页面列表',component: page_list,hidden:false},
      {path:'/cms/page/edit/:pageId',name:'',component:page_edit,hidden:false},
      {path:'/cms/page/add',name:'',component:page_add,hidden:false},
    ]
  }
]
