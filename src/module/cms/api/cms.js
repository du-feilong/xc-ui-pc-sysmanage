import http from './../../../base/api/public'
import querystring from 'querystring'
let sysConfig = require('@/../config/sysConfig')
let apiUrl = sysConfig.xcApiUrlPre;

//页面查询
export const page_list = (page,size,params) =>{
  //请求服务端的页面查询接口
  return http.requestPost(apiUrl+'/cms/page/list/'+page+'/'+size,params);
}

//根据页面id查询页面详情
export const page_get = (pageId) => {
  return http.requestQuickGet(apiUrl+'/cms/page/findById/'+pageId);
}

//编辑页面
export const page_edit = (pageId,params) => {
  return http.requestPost(apiUrl+'/cms/page/edit/'+pageId,params);
}

//删除页面
export const page_del = (pageId) => {
  return http.requestDelete(apiUrl+'/cms/page/delete/'+pageId);
}

//添加页面
export const page_add = (params) => {
  return http.requestPost(apiUrl+'/cms/page/add',params);
}